---
title: "Kako so sovjetski šahisti prišli do več denarja?"
date: 2023-09-25T12:00:17+02:00
avtorji: ["Matija Sirk"]
summary: "Poleti leta 1955 je bil v Moskvi meč med Sovjetsko zvezo in ZDA. Prisoten je bil tudi Hruščov. Vprašal je velemojstra Taimanova..."
---

![Meč ZDA vs Sovjetska zveza 1955](images/mec_zda_sovjetska_1955.jpeg)

Poleti leta 1955 je bil v Moskvi meč med Sovjetsko zvezo in ZDA. Prisoten je bil tudi Hruščov. Vprašal je velemojstra Taimanova: "Ali sovjetski igralci dobite kakšen denar, ko igrate v tujini?"

"Seveda ne", je odvrnil Taimanov. "Kako bi lahko sprejeli denar od buržoazije? Naš cilj je pokazati prednosti socialistične družbe in dokazati, da smo močnejši kot oni."

"A ko igrate doma, ste pa plačani?"

"Seveda, kako bi drugače preživeli?"

Hruščov se je zamislil. "Nekaj je narobe," je končno rekel. "Poglej, od kapitalistov, ki imajo polno denarja, ne vzamete nič. Jemljete pa denar naši revni državi. To ni sprejemljivo. Njim morate vzeti denar. In več ko jim ga vzamete, tem bolje!"

Čez nekaj dni je Športni komite izdal poseben ukaz, s katerim je bilo sovjetskim šahistom dovoljeno prejeti plačila v tuji valuti za igro v tujini. Tako so Sovjetski šahisti dobili dovoljenje za bogatenje.

📸 Meč ZDA vs Sovjetska zveza 1955, 4. julij pred ameriško ambasado v Sovjetski zvezi.

Izvirnik: [@chessinstories via Twitter](https://twitter.com/chessinstories/status/1706059414711513316/photo/1)
