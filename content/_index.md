---
title: "Dobrodošli!"
---

> Šah je borba proti samemu sebi.
> Nikoli ne igram proti nasprotniku, igram proti figuram.
> {{< qcite >}}— velemojster [Svetozar Gligorić](https://sl.wikipedia.org/wiki/Svetozar_Gligori%C4%87){{< /qcite >}}

Če si nekaj bolj izkušenih kolegov ne bi vzelo časa zame, ko sem s šahom začenjal, se verjetno igri na dolgi rok ne bi posvetil. Tako bi zamudil mnogo novih poznanstev, sladkih zmag, pa tudi grenkih porazov. Kako naj se jim drugače zahvalim, kot pa da poskusim to igro prikupiti še komu? Dragi bralec, ta stran je prvi korak.

Hkrati mi šah dostikrat teži misli. Včasih mi prav ne pusti spati. Kombinacija dolgih ur osamljenih priprav, občasne prijateljske partije in kresanja proverbialnih mečev na turnirjih ni vedno lahka. Spet, srečavanja s starimi znanci jo olajšajo — kljub temu vsakemu turnirju sledi ponovno zapiranje v priprave. Tudi tako nerodna komunikacija kot jo omogoča splet je vendarle komunikacija. Dragi bralec, tudi temu služi ta stran.

> Če misliš, ne da bi ob tem pisal, samo misliš, da misliš.
> {{< qcite >}}— doktor [Leslie Lamport](https://en.wikipedia.org/wiki/Leslie_Lamport){{< /qcite >}}

Šah je simulacija pravilnega mišljenja. V njem zmaga tisti, ki bolj precizno premisli situacijo na šahovnici. Nobena priprava se tako ne more primerjati s turnirsko partijo. Ta je namreč izredno hitra povratna zanka, kjer vsaka malce neprecizna misel "faše" svojo kazen. Bojda je pa pisanje blizu, saj so tudi kritike bralcev tovrstne zanke, četudi mnogo počasnejše. Dragi bralec, zato ta stran.

Ključni vidik je pa seveda praktični: Vsak šahist skozi čas gotovo naleti na nešteto pozicij, idej, delcev partij, ki ga zagrabijo, ali pa ki le nazorno ponazorijo nek vidik igre. Takšne pozicije je treba deliti in hudo nepraktično je brskati po spominu za njimi. Dragi bralec, oprosti mi sebičnost: diagrami na tej strani so, da bi bili meni pri roki, a upam, da ti bodo vseeno zanimivi.

_— Matija_
