---
title: Kolofon
toc: true
summary: "Z hvaležnostjo sem med izdelavo te strani uporabil: ..."
---

## Grafični elementi

Ikona strani pretvorjena iz [SVG REPO: "Chess clock"](https://www.svgrepo.com/svg/11165/chess-clock).

## Pisave

### Naslovi: EB Garamond

[EB Garamond](https://fonts.google.com/specimen/EB+Garamond?query=eb+garamond) sta Georg Duffner in Octavio Pardo ustvarila na podlagi pisave Conrada Bernerja in Christiana Egenolffa iz leta 1592. Črke z zgodovino, ni kaj.

### Tekst: NotoSans

V tekstu uporabljam [NotoSans](https://fonts.google.com/noto), skupaj z njegovo poševno in simboli 2 različico.

## Tehnologija

Stran poganja odprtokodni generator statičnih strani [Hugo](https://gohugo.io/). Gostovana je na [Gitlabu](https://gitlab.com/sirkmatija/rokada.si), kjer lahko tudi najdete njeno kodo.

Diagrame, analizne plošče in ostalo šahovsko vsebino poganja [Lichess](https://lichess.org), najboljša spletna aplikacija za igranje šaha.

Za vašo zasebnost med obiskom te strani skrbi [Let’s Encrypt-ov](https://letsencrypt.org/) certifikat.

## Dizajn

Sprotne opombe je inspirala njihova raba v delih [doktorja Edwarda Tufteja](https://en.wikipedia.org/wiki/Edward_Tufte). Njihovo vsebino pa kreativna raba opomb v delih [sira Terrya Pratchetta](https://en.wikipedia.org/wiki/Terry_Pratchett).
