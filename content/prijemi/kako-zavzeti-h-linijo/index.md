---
title: "Kako zavzeti h-linijo"
date: 2023-10-03T12:20:17+02:00
avtorji: ["Matija Sirk"]
summary: 'Po napeti partiji med Gligorićem in Matanovićem se je pozicija zjasnila. Da bi beli lahko dokazal svojo prednost pa mora najprej prevzeti h-linijo. Za to uporabi "šolski" manever...'
---

![r7/4rbk1/3q1p2/p1pPpBp1/1pP1Q3/1P6/P4PP1/3RR1K1 w - - 0 34](images/33b.gif)

Po napeti partiji med [Gligorićem in Matanovićem (Beograd, 1964)](https://lichess.org/O4DUxiYx) se je pozicija zjasnila.
Da bi beli lahko dokazal svojo prednost pa mora najprej prevzeti _h-linijo_. Za to uporabi "šolski" manever:

**34\. g3 Tee8 35. Kg2 Th8 36. Th1 Txh1 37. Txh1 Th8 38. Txh8 Kxh8**

![7k/5b2/3q1p2/p1pPpBp1/1pP1Q3/1P4P1/P4PK1/8 w - - 0 39](images/38b.gif)

Najhitrejši način prevzema _h-linije_ je paradoksalna:

**39\. Kg1! Kg7 40. Dh1**

![8/5bk1/3q1p2/p1pPpBp1/1pP5/1P4P1/P4P2/6KQ b - - 3 40](images/40w.gif)

In prednost belega je jasna.
