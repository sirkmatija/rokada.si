---
title: "Gukesh predal remi končnico!"
date: 2023-10-03T12:30:17+02:00
avtorji: ["Matija Sirk"]
summary: "Danes se je na azijskih igrah partija med Parhamom in Gukeshom končala hudo nepričakovano. V topniški končnici je z zgolj 40 sekundami na uri Gukesh predal partijo. Na prvi pogled je pozicija zanj res hudo težka, ampak ali je res izgubljena?"
---

![Parham Maghsoodloo na Tata Steel 2023](images/parham_2023.jpg "Parham Maghsoodloo na Tata Steel 2023. [Wikipedija (Frans Peeters)](https://en.wikipedia.org/wiki/Gukesh_D#/media/File:Dommaraju_Gukesh_in_2023.jpg)")

Danes se je na azijskih igrah partija med Parhamom in Gukeshom končala hudo nepričakovano. V topniški končnici je z zgolj 40 sekundami na uri Gukesh predal partijo. Na prvi pogled je pozicija zanj res hudo težka, ampak ali je res izgubljena?

8/6R1/2r1k1pK/5p1p/5P1P/6P1/8/8 b - - 8 64

Problem črnega je, da ne more braniti kmetov na g- in h- liniji.

![Dommaraju Gukesh na Tata Steel 2023](images/gukesh_2023.jpg "Dommaraju Gukesh na Tata Steel 2023. [Wikipedija (Frans Peeters)](https://en.wikipedia.org/wiki/Gukesh_D#/media/File:Dommaraju_Gukesh_in_2023.jpg).")

Po [analizi Sagarja Shaha](https://www.youtube.com/watch?t=14024&v=MAhSYmPmf5s&feature=youtu.be), se izkaže da črni lahko preživi.

Beli preti Txg6+ s prehodom v dobljeno kmečko končnico, zato mora črni nujno umakniti topa z 6. vrste:

64. ... Tc3
65. Txg6+ Kf7
66. Kxh5

8/5k2/6R1/5p1K/5P1P/2r3P1/8/8 b - - 0 66

Bistvena značilnost pozicije je: če umaknemo s plošče črnega f- in belega g- kmeta, dobimo znano remi pozicijo. To je že v 40. letih prejšnjega stoletja analiziral [Botvinik](https://sl.wikipedia.org/wiki/Mihail_Botvinik).

8/5k2/6R1/7K/5P1P/2r5/8/8 b - - 0 66

> Od Mihaila Botvinika se lahko naučimo, da se je treba resno pripraviti na vsak dvoboj. Za primer: jaz sem bil Botvinikov sekundant pred svetovnim
> prvenstvom leta 1948. V svoj študijski program je Botvinik vključil študij vseh topniških končnic z f- in h- kmetoma. To me je šokiralo. Zakaj jih je
> vključil? Takšno končnico dobi človek le enkrat v življenju. "Ne, ne zaslužim si postati svetovni prvak, če jih ne poznam," je rekel Botvinik. Jaz pa
> sem moral poiskati vse primere te končnice!
> {{< qcite >}}— Velemojster [Salomon Fohr](https://en.wikipedia.org/wiki/Salo_Flohr) za FIDE review #3, 1961.{{< /qcite >}}

Zato je jasno, da dokler črni top napada g- kmeta ne more beli kralj na g5 (saj prekrije topovo obrambo kmeta), niti ne more beli top dol z g-linije. Tako ima beli na voljo le poskus prodora po h- liniji.

**66. ... Tb3 67. Kh6 Tc3 **

Črni s topom čaka na tretji vrsti, da drži grožnjo na g- kmeta.

**68. h5 Tb3 69. Tg7+ Kf6 70. Kh7 Tc3 71. h6 Tb3 **

Beli kmet ne more napredovati, če se kralj ne umakne s h- linije. A v tem primeru:

**72. Kg8 Tb8+ **

In napredek za belega ni možen.
