+++
title = '{{ replace .File.ContentBaseName "-" " " | title }}'
date = {{ .Date }}
draft = true
avtorji =  [
{{- $authors := .Site.Params.defaultAvtorji -}}
{{- range $index, $author := $authors }}
  "{{ $author }}"{{- if ne (add $index 1) (len $authors) -}},{{ end }}
{{- end }}
]
+++
