function reflow() {
  const summaryContainer = document.querySelector("#summaries");
  const sidenoteContainer = document.querySelector("#sidenotes");

  const isLargeScreen =
    summaryContainer &&
    window.getComputedStyle(summaryContainer).display !== "none";

  const hasSummaryContainer = !!summaryContainer;
  const hasSidenoteContainer = !!sidenoteContainer;

  /**
   * Premakni povzetke v njihov stolpec in jih poravna s paragrafi.
   */
  function reflowSummaries() {
    if (isLargeScreen) {
      const summaries = document.querySelectorAll(".summary");

      for (const summary of summaries) {
        const parent = summary.parentElement;

        // Naredimo prazne elemente.
        // Da lahko v primeru resize preračunamo pozicijo.
        const ref = document.createElement("span");
        ref.className = "summary-ref";

        parent.replaceChild(ref, summary);

        summaryContainer.appendChild(summary);
      }
    } else {
      // Summary-e vrnemo nazaj v tekst.
      const summaries = document.querySelectorAll(".summary");
      const summaryRefs = document.querySelectorAll("main .summary-ref");

      summaries.forEach((summary, index) => {
        const ref = summaryRefs[index];

        summary.parentElement.removeChild(summary);
        ref.parentElement.replaceChild(summary, ref);
        summary.style.top = `0px`;
      });
    }
  }

  /**
   * Premakni sprotne opombe v njihov stolpec.
   */
  function reflowSidenotes() {
    const sidenotes = document.querySelectorAll(".sidenote-raw");

    sidenotes.forEach((sidenoteRaw, index) => {
      const no = index + 1;

      const ref = document.createElement("a");
      ref.className = "sidenote-ref";
      ref.innerText = `[${no}]`;

      if (!isLargeScreen) {
        ref.href = `#sidenote-${no}`;
        ref.id = `sidenote-ref-${no}`;
      }

      sidenoteRaw.parentElement.replaceChild(ref, sidenoteRaw);

      // Div-e tvorimo tukaj - če so znotraj shortcode, naredi hugo kolaps sistema s paragraph breakom.
      const sidenote = document.createElement("div");
      sidenote.className = "sidenote";

      const sidenoteContent = document.createElement("div");
      sidenoteContent.className = "sidenote-content";
      sidenoteContent.innerHTML = sidenoteRaw.innerHTML;

      const backref = document.createElement("a");
      backref.className = "sidenote-ref";
      backref.innerText = `[${no}]`;

      if (!isLargeScreen) {
        backref.href = `#sidenote-ref-${no}`;
        backref.id = `sidenote-${no}`;
      }

      sidenote.appendChild(backref);
      sidenote.appendChild(sidenoteContent);

      if (!isLargeScreen) {
        const backrefTail = document.createElement("a");
        backrefTail.className = "sidenote-ref";
        backrefTail.innerText = "↸";
        backrefTail.href = `#sidenote-ref-${no}`;
        sidenote.appendChild(backrefTail);
      }

      sidenoteContainer.appendChild(sidenote);
    });
  }

  /**
   * Poravna robne opombe z njihovimi markerji.
   */
  function positionSidenotes() {
    const sidenotes = document.querySelectorAll(".sidenote");
    const sidenoteRefs = document.querySelectorAll("main .sidenote-ref");

    sidenotes.forEach((sidenote, index) => {
      const ref = sidenoteRefs[index];

      sidenote.style.top = `${ref.offsetTop}px`;
    });
  }

  /**
   * Poravna povzeke z njihovimi markerji.
   */
  function positionSummaries() {
    const summaries = document.querySelectorAll(".summary");
    const summaryRefs = document.querySelectorAll("main .summary-ref");

    summaries.forEach((summary, index) => {
      const ref = summaryRefs[index];

      summary.style.top = `${ref.offsetTop}px`;
    });
  }

  if (hasSidenoteContainer) {
    reflowSidenotes();
  }

  if (hasSummaryContainer) {
    reflowSummaries();
  }

  // Pozicioniramo šele po reflowu, saj lahko umik opomb iz teksta tega zamakne.

  if (isLargeScreen && hasSummaryContainer) {
    positionSummaries();
  }

  if (isLargeScreen && hasSidenoteContainer) {
    positionSidenotes();
  }
}

addEventListener("load", reflow);
addEventListener("resize", reflow);
